#include "Motor.h"
#include <iostream>
#include "HardwareAdapter.h"

#include <QtConcurrent/QtConcurrent>


Motor::Motor()
{
    m_posCounter = 0;
    m_negCounter = 0;
    m_doubleCnt = 0;

    HardwareAdapter::getInstance()->motorController()->registerMotor(this);

}

double Motor::currentPosition() const
{
    return m_currentPosition;
}

void Motor::setCurrentPosition(double newCurrentPosition, double velocity, uint64_t time)
{


    m_currentPosition = newCurrentPosition;
    m_currentMotorPulse = m_controlAlgorithm.pushSignalData(m_currentPosition, velocity, time);

    m_currentStat = m_controlAlgorithm.pulseStat();

    if(m_currentStat == PULSE_POSITIVEPULSE){
        m_posCounter++;
        HardwareAdapter::getInstance()->motorController()->doPulse(this, true);
    }else if(m_currentStat == PULSE_NEGATIVEPULSE){
        HardwareAdapter::getInstance()->motorController()->doPulse(this, false);
    }


    m_prevMotorPulse = m_currentMotorPulse;

/*
    if(m_prevAbsMotorPulseCount != m_controlAlgorithm.getAbsPulseCount()){

       // qDebug() << ".";

        sendCommand();
       // auto feature = QtConcurrent::run( &Motor::sendCommand, this);
        //qDebug() << ".";

    }
     m_prevAbsMotorPulseCount = m_controlAlgorithm.getAbsPulseCount();
*/
}

double Motor::currentVelocity() const
{
    return m_currentVelocity;
}

void Motor::setCurrentVelocity(double newCurrentVelocity)
{
    m_currentVelocity = newCurrentVelocity;
}

double Motor::previousPosition() const
{
    return m_previousPosition;
}

void Motor::setPreviousPosition(double newPreviousPosition)
{
    m_previousPosition = newPreviousPosition;
}

const std::vector<MotorPulse> &Motor::getPulses() const
{
   // m_pulses = m_controlAlgorithm.getPulses();
    return m_controlAlgorithm.getPulses();
}

const MotorControlAlgorithm &Motor::getControlAlgorithm() const
{
    return m_controlAlgorithm;
}

const MotorPulse &Motor::getCurrentMotorPulse() const
{
    return m_currentMotorPulse;
}

int Motor::getCurrentTotalMotorPulseCount()
{
    return m_controlAlgorithm.getPulseCount();
}

void Motor::setControllerIndex(int newControllerIndex)
{
    m_controllerIndex = newControllerIndex;
}

int Motor::getAbsMotorPulseCount()
{
    return m_controlAlgorithm.getAbsPulseCount();
}

int Motor::controllerIndex() const
{
    return m_controllerIndex;
}

void Motor::sendCommand()
{
    //qDebug() << "-";
    HardwareAdapter::getInstance()->motorController()->goToMotorPosition(this);


}

PulseStat Motor::currentStat() const
{
    return m_currentStat;
}

void Motor::setStepRatio(double stepRatio)
{
    std::cout << "Motor stepRatio: " << stepRatio << std::endl;

    m_controlAlgorithm.setStepRatio(stepRatio);
}
