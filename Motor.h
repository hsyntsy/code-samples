#ifndef MOTOR_H
#define MOTOR_H

#include <vector>
#include "algorithm/MotorControlAlgorithm.h"

/**
 * @brief Abstract class for Motor. Contains common functions of SmallMotor and LargeMotor
 *
 */
class Motor
{
public:
    Motor();
    double currentPosition() const;
    void setCurrentPosition(double newCurrentPosition, double velocity, uint64_t time);

    double currentVelocity() const;
    void setCurrentVelocity(double newCurrentVelocity);

    double previousPosition() const;
    void setPreviousPosition(double newPreviousPosition);

    const std::vector<MotorPulse> &getPulses() const;

    const MotorControlAlgorithm &getControlAlgorithm() const;

    const MotorPulse &getCurrentMotorPulse() const;
    void setStepRatio(double stepRatio);

    int getCurrentTotalMotorPulseCount();

    void setControllerIndex(int newControllerIndex);

    int getAbsMotorPulseCount();

    int controllerIndex() const;

    PulseStat currentStat() const;

private:


    /**
     * @brief Buffer for storing previous position value. Useful for calculating the velocity.
     */
    double m_previousPosition;
    int m_currentPos = 0;
    int m_prevAbsMotorPulseCount = 0;

    short m_doubleCnt = 0;


    double m_currentPosition;
    double m_currentVelocity;

    void sendCommand();

    /**
     * @brief Storing all the pulses of the motor for all runtime.
     */
    std::vector<MotorPulse> m_pulses;
    MotorControlAlgorithm m_controlAlgorithm;
    MotorPulse m_currentMotorPulse, m_prevMotorPulse;
    int m_controllerIndex = -1;
    int m_posCounter = 0;
    int m_negCounter = 0;
    PulseStat m_currentStat = PULSE_NOPULSE;

};

#endif // MOTOR_H
