#ifndef MOTORCONTROLLER_H
#define MOTORCONTROLLER_H

#include <string>
#include <vector>
#include "Motor.h"

class MotorController
{
public:
    MotorController();
    virtual bool connect(std::string address) = 0;
    virtual bool initilize() = 0;
    virtual bool registerMotor(Motor *motor) = 0;
    virtual void goToMotorPosition(Motor *motor) = 0;
    virtual void doPulse(Motor* motor, bool isPositive = true) = 0;
    virtual void do10Pulse(Motor* motor, bool isPositive = true) = 0;
    virtual void doDoublePulse(Motor* motor, bool isPositive) = 0;
    virtual void doPulseForAllRegisteredMotors() = 0;
    virtual bool isMoving() const = 0;


protected:
    std::vector<Motor*> m_registredMotors;

};

#endif // MOTORCONTROLLER_H
