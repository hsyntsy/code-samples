#include "GalilMotorController.h"
#include <stdlib.h>
#include <stdio.h>

inline void e(GReturn rc)
{
    if (rc != G_NO_ERROR)
        throw rc;
}




GalilMotorController::GalilMotorController()
{

}

bool GalilMotorController::connect(std::string address)
{

    qDebug() << "-----------------";
    e(GOpen(GCStringIn(address.c_str()), &g)); //Opens a connection at the provided address
    qDebug() << "-----------------";

    return true;
}

bool GalilMotorController::initilize()
{

    qDebug() << "----------8-------";

    e(GCommand(g, "MT -2.5,-2.5,-2.5,-2.5,-2.5,-2.5;", buf, G_SMALL_BUFFER, &read_bytes));
    qDebug() << "-----------------";

    e(GCommand(g, "MT ?,?,?,?;", buf, G_SMALL_BUFFER, &read_bytes));
    qDebug() << "<<MT ?,?,?,?;: " << buf << ">>\n";

    e(GCmd(g, "ST ABCDEF;"));    // stop motors
    e(GMotionComplete(g, "A")); //Wait for motion to complete
    e(GMotionComplete(g, "B")); //Wait for motion to complete
    e(GMotionComplete(g, "C")); //Wait for motion to complete
    e(GMotionComplete(g, "D")); //Wait for motion to complete
    e(GMotionComplete(g, "E")); //Wait for motion to complete
    e(GMotionComplete(g, "F")); //Wait for motion to complete
    e(GCmd(g, "DP 0,0,0,0,0,0;")); // Start position at absolute zero
    e(GCmd(g, "PT 1,1,1,1,1,1;")); // Start position tracking mode on A axis
/*

    // e(GCmd(g, "SHA"));   // Set servo here

    e(GCmd(g, "DP 0,0,0,0,0,0;")); // Start position at absolute zero
    //e(GCmd(g, "PT 1,1,1,1,1,1;")); // Start position tracking mode on A axis
    e(GCmd(g, "PTA=1;")); // Start position tracking mode on A axis
*/
    int speed = 2000000;
    int acc = 100 * speed; //set acceleration/deceleration to 100 times speed

    sprintf(buf, "SP %d,%d,%d,%d,%d,%d;", speed, speed, speed, speed, speed, speed);
    e(GCmd(g, buf)); // Set speed
    qDebug() << "-----------------";

    sprintf(buf, "AC %d,%d,%d,%d,%d,%d;", acc, acc, acc, acc, acc, acc);
    e(GCmd(g, buf)); // Set acceleration
    qDebug() << "-----------------";

    sprintf(buf, "DC %d,%d,%d,%d,%d,%d;", acc, acc, acc, acc, acc, acc);
    e(GCmd(g, buf)); // Set acceleration
    qDebug() << "-----------------";



    m_counter = 0;
    return true;

}

bool GalilMotorController::registerMotor(Motor *motor)
{
    qDebug() << "Motor registered: " << m_registredMotors.size() << "  with the address: " << motor;
    motor->setControllerIndex(m_registredMotors.size());
    m_registredMotors.push_back(motor);
    return true;

}

void GalilMotorController::goToMotorPosition(Motor* motor)
{
    QChar motorId = '.';
    switch(motor->controllerIndex()){
        case 0:
            motorId = 'A';
            break;
        case 1:
            motorId = 'B';
            break;
        case 2:
            motorId = 'C';
            break;
        case 3:
            motorId = 'D';
            break;
        case 4:
            motorId = 'E';
            break;
        case 5:
            motorId = 'F';
            break;
        default:
            qDebug() << "GalilMotorController error: unregistered motor...";

    }
    QString comS = "PA" + QString(motorId) + "=" + QString::number(motor->getAbsMotorPulseCount()) + ";";
    std::string cComS = comS.toStdString();

   // if(motor->getAbsMotorPulseCount() > 4500)

    GCStringIn com = cComS.c_str();
 //   qDebug() << "GCStringIn com: " << com;



    GCommand(g, com, buf, G_SMALL_BUFFER, &read_bytes);
    //GCommand(g, "WT ABCDEFG", buf, G_SMALL_BUFFER, &read_bytes);
    //GCommand(g, "WT 1", buf, G_SMALL_BUFFER, &read_bytes);



}

void GalilMotorController::doPulse(Motor* motor, bool isPositive)
{


    //QChar motorId = '.';
    switch(motor->controllerIndex()){
    case 0:
            if(isPositive){


                GCommand(g, "IPA=1", buf, G_SMALL_BUFFER, &read_bytes);


            }else{


                GCommand(g, "IPA=-1", buf, G_SMALL_BUFFER, &read_bytes);


            }
            break;



    case 1:
            if(isPositive){


                GCommand(g, "IPB=1", buf, G_SMALL_BUFFER, &read_bytes);


            }else{


                GCommand(g, "IPB=-1", buf, G_SMALL_BUFFER, &read_bytes);

            }
            break;



    case 2:
            if(isPositive){


                GCommand(g, "IPC=1", buf, G_SMALL_BUFFER, &read_bytes);


            }else{


                GCommand(g, "IPC=-1", buf, G_SMALL_BUFFER, &read_bytes);

            }
            break;



    case 3:
            if(isPositive){


                GCommand(g, "IPD=1", buf, G_SMALL_BUFFER, &read_bytes);


            }else{


                GCommand(g, "IPD=-1", buf, G_SMALL_BUFFER, &read_bytes);

            }
            break;



    case 4:
            if(isPositive){


                GCommand(g,  "IPE=1", buf, G_SMALL_BUFFER, &read_bytes);


            }else{


                GCommand(g, "IPE=-1", buf, G_SMALL_BUFFER, &read_bytes);

            }
            break;



    case 5:
            if(isPositive){


                GCommand(g,  "IPF=1", buf, G_SMALL_BUFFER, &read_bytes);


            }else{


                GCommand(g, "IPF=-1", buf, G_SMALL_BUFFER, &read_bytes);

            }
            break;
    default:
            return;
            //qDebug() << "GalilMotorController error: unregistered motor...";

    }







}

void GalilMotorController::do10Pulse(Motor *motor, bool isPositive)
{

    //QChar motorId = '.';
    switch(motor->controllerIndex()){
    case 0:
            if(isPositive){


                GCommand(g, "IPA=10", buf, G_SMALL_BUFFER, &read_bytes);


            }else{


                GCommand(g, "IPA=-10", buf, G_SMALL_BUFFER, &read_bytes);


            }
            break;



    case 1:
            if(isPositive){


                GCommand(g, "IPB=10", buf, G_SMALL_BUFFER, &read_bytes);


            }else{


                GCommand(g, "IPB=-10", buf, G_SMALL_BUFFER, &read_bytes);

            }
            break;



    case 2:
            if(isPositive){


                GCommand(g, "IPC=10", buf, G_SMALL_BUFFER, &read_bytes);


            }else{


                GCommand(g, "IPC=-10", buf, G_SMALL_BUFFER, &read_bytes);

            }
            break;



    case 3:
            if(isPositive){


                GCommand(g, "IPD=10", buf, G_SMALL_BUFFER, &read_bytes);


            }else{


                GCommand(g, "IPD=-10", buf, G_SMALL_BUFFER, &read_bytes);

            }
            break;



    case 4:
            if(isPositive){


                GCommand(g,  "IPE=10", buf, G_SMALL_BUFFER, &read_bytes);


            }else{


                GCommand(g, "IPE=-10", buf, G_SMALL_BUFFER, &read_bytes);

            }
            break;



    case 5:
            if(isPositive){


                GCommand(g,  "IPF=10", buf, G_SMALL_BUFFER, &read_bytes);


            }else{


                GCommand(g, "IPF=-10", buf, G_SMALL_BUFFER, &read_bytes);

            }
            break;
    default:
            return;
            //qDebug() << "GalilMotorController error: unregistered motor...";

    }




}




void GalilMotorController::doDoublePulse(Motor* motor, bool isPositive)
{

    QChar motorId = '.';
    switch(motor->controllerIndex()){
    case 0:
        motorId = 'A';
        break;
    case 1:
        motorId = 'B';
        break;
    case 2:
        motorId = 'C';
        break;
    case 3:
        motorId = 'D';
        break;
    case 4:
        motorId = 'E';
        break;
    case 5:
        motorId = 'F';
        break;
    default:
        qDebug() << "GalilMotorController error: unregistered motor...";

    }


    if(isPositive){
        m_counter++;
        //QString comS = "IP" + QString(motorId) + "=1";

        std::string cComS = "IP" + QString(motorId).toStdString() + "=4";
        // qDebug() << "std::string cComS: " << cComS;

        GCStringIn com = cComS.c_str();
        // qDebug() << "GCStringIn com: " << com;


        GCommand(g, com, buf, G_SMALL_BUFFER, &read_bytes);



    }else{
        QString comS = "IP" + QString(motorId) + "=-4";
        std::string cComS = comS.toStdString();
        GCStringIn com = cComS.c_str();
        qDebug() << "GCStringIn com: " << com;



        GCommand(g, com, buf, G_SMALL_BUFFER, &read_bytes);


    }




}

void GalilMotorController::doPulseForAllRegisteredMotors()
{
    m_isMoving = true;
    std::string command = "IP ";
    for(int i = 0; i < m_registredMotors.size(); i++){
        Motor *motor = m_registredMotors.at(i);
        PulseStat stat = motor->currentStat();


        switch(stat){
            case PULSE_POSITIVEPULSE:
                command = command + "1";
                //if(i != m_registredMotors.size()-1){
                    command = command + ",";
                //}
                break;

            case PULSE_NEGATIVEPULSE:
                command = command + "-1";
               // if(i != m_registredMotors.size()-1){
                    command = command + ",";
               // }
                break;

            case PULSE_NOPULSE:
                command = command + "0";

                //if(i != m_registredMotors.size()-1){
                    command = command + ",";
               // }
                break;

            default:
                return;
        }//switch

    } //for motors

    m_cnt++;
    command = command + "0,0;";
    GCStringIn com = command.c_str();



    GCommand(g, com, buf, G_SMALL_BUFFER, &read_bytes);

    qDebug() << "command: " << command;

    //qDebug() << "command: " << command;
    m_isMoving = false;

}

bool GalilMotorController::isMoving() const
{
    return m_isMoving;
}


void MotorControlAlgorithm::sendCommand()
{

}
