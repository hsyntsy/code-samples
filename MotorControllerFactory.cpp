#include "MotorControllerFactory.h"

MotorControllerFactory::MotorControllerFactory()
{

}

MotorController *MotorControllerFactory::generateMarkerTracker(MotorControllerType type)
{
    if (type == MotorController_Galil)
        return new GalilMotorController();
    else if(type == MotorController_Simulation)
        return new SimulationMotorController();
    else
        return nullptr;
}
