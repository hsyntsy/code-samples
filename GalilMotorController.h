#ifndef GALILMOTORCONTROLLER_H
#define GALILMOTORCONTROLLER_H

#include "MotorController.h"


#include "gclib.h"
#include "gclibo.h"


class GalilMotorController : public MotorController
{
public:
    GalilMotorController();
    virtual bool connect(std::string address) override;
    virtual bool initilize() override;
    virtual bool registerMotor(Motor *motor) override;
    virtual void goToMotorPosition(Motor* motor) override;
    virtual void doPulse(Motor* motor, bool isPositive) override;
    virtual void do10Pulse(Motor* motor, bool isPositive) override;
    virtual void doDoublePulse(Motor* motor, bool isPositive) override;

    virtual void doPulseForAllRegisteredMotors() override;

    virtual bool isMoving() const override;

private:

    GCon g = 0;
    GSize read_bytes = 0; //bytes read in GCommand
    char buf[G_SMALL_BUFFER]; //traffic buffer
    int m_counter = 0;
    int m_cnt = 0;

    bool m_isMoving = false;

};

#endif // GALILMOTORCONTROLLER_H
