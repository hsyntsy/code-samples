
#ifndef SIMULATIONMOTORCONTROLLER_H
#define SIMULATIONMOTORCONTROLLER_H

#include "MotorController.h"



class SimulationMotorController : public MotorController
{
public:
    SimulationMotorController();

    virtual bool connect(std::string address) override;
    virtual bool initilize() override;
    virtual bool registerMotor(Motor *motor) override;
    virtual void goToMotorPosition(Motor* motor) override;
    virtual void doPulse(Motor* motor, bool isPositive) override;
    virtual void do10Pulse(Motor* motor, bool isPositive) override;
    virtual void doDoublePulse(Motor* motor, bool isPositive) override;

    virtual void doPulseForAllRegisteredMotors() override;

    virtual bool isMoving() const override;

};

#endif // SIMULATIONMOTORCONTROLLER_H
