#ifndef MOTORCONTROLLERFACTORY_H
#define MOTORCONTROLLERFACTORY_H

#include <MotorController.h>
#include "GalilMotorController.h"
#include "SimulationMotorController.h"

enum MotorControllerType {
    MotorController_Galil,
    MotorController_Simulation
};


class MotorControllerFactory
{
public:
    MotorControllerFactory();
    MotorController *generateMarkerTracker(MotorControllerType type);

};

#endif // MOTORCONTROLLERFACTORY_H
