#ifndef LARGEMOTOR_H
#define LARGEMOTOR_H

#include "Motor.h"

/**
 * @brief Child class of Motor. Has been created and used for future changes between small motors and large motors.
 */
class LargeMotor : public Motor
{
public:
    LargeMotor();
};

#endif // LARGEMOTOR_H
