#ifndef SMALLMOTOR_H
#define SMALLMOTOR_H

#include "Motor.h"

/**
 * @brief Child class of Motor. Has been created and used for future changes between small motors and large motors.
 */
class SmallMotor : public Motor
{
public:
    SmallMotor();
};

#endif // SMALLMOTOR_H
